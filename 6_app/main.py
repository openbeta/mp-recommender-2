from flask import Flask, request, jsonify
from flask_cors import CORS
import json, pickle
from json_handler_v1 import JsonHandler

app = Flask(__name__)
CORS(app)

@app.route('/', methods = ['GET','POST'])
def return_json():
    if request.method == 'POST':
        inc_json = request.get_json()
        print(inc_json)
        try:
            id = inc_json['user_id']
            print('Return 10 Recommendations for {}'.format(id))
            return handler1.run_handler(inc_json)
        except:
            print("ERROR, incoming json:")
            print(inc_json)
            return jsonify({"message":"no routes found"})
    else:
        return jsonify({"message":"OK"})

handler_pkl_loc = '../models/handler_v1.pkl'
handler1 = pickle.load(open(handler_pkl_loc, 'rb'))

if __name__ == "__main__":
    #handler_pkl_loc = '/Users/colinbrochard/ProjRepos/MtProj/6_app/ignore/handler_v1.pkl'
    app.run(host="0.0.0.0", port=8080)
    #('/home/ec2-user/MtProj/6_app/cert.pem','/home/ec2-user/MtProj/6_app/key.pem'))
