#!/bin/bash
unzip models/handler_v1.pkl.zip -d models
pipenv run gunicorn main:app --chdir 6_app --bind 0.0.0.0:8080 --workers 2